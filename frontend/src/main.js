import Vue from 'vue'
import BootstrapVue from 'bootstrap-vue'
import VueRouter from 'vue-router'

import App from './App.vue'

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

Vue.use(BootstrapVue)
Vue.use(VueRouter);

Vue.use(function (Vue, options) {
    Vue.bus = new Vue()
    Vue.prototype.$bus = Vue.bus
    Object.defineProperty(Vue.prototype, '$app', {
        get() {
            return this.$root.$children[0]
        }
    })
})


const routes = [
    // { path: '/', component: require('./Home.vue').default },
    { path: '/login', component: require('./Login.vue').default },
    { path: '/api-keys', component: require('./ApiKeys.vue').default, meta: { title: "My Api Keys", requiresLogin: true } }
    // { path: '/_new', component: require('./backend/New.vue').default },
    // { path: '/_edit/:urlKey', component: require('./backend/Edit.vue').default },
    // { path: '/:urlKey', component: require('./Post.vue').default }
]

const router = new VueRouter({
    routes,
    linkActiveClass: 'active',
    mode: 'history'
})

router.beforeEach(function(to, from, next) {
    const isLoggedIn = vue && vue.$children[0].isLoggedIn
    if (to.meta.requiresLogin && !isLoggedIn) {
        next('/login')
    } else {
        next()
    }
})

const vue = new Vue({
    router,
    el: '#app',
    render: h => h(App)
})
