import axios from 'axios'

const client = axios.create({
    baseURL: 'http://icw-672-saas.makrohost-server.de/'
})

export function login(username, password) {
    return client.post('/login', { key: username, secret: password })
    .then(function(response) {
        if (response.data.roles.indexOf('account') < 0) {
            return false
        } else {
            client.defaults.headers['X-Access-Token'] = response.data.token
            return response.data.token
        }
    })
    .catch(function(err) {
        return false
    })
}

export function getMe() {
    return client.get('/account/v1/me').then(res => res.data)
}

export function getApiKeys() {
    return client.get('/account/v1/api-keys').then(res => res.data.data)
}

export function addApiKey(name, role) {
    return client.post('/account/v1/api-key', {
        name,
        roles: [ role ]
    }).then(res => res.data)
}

export function removeApiKey(key) {
    return client.delete('/account/v1/api-key/' + key)
    .then(res => res.status == 200)
    .catch(err => false)
}
