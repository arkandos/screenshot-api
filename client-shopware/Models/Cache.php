<?php
namespace SysgMediaCapture\Models;

use Symfony\Component\Validator\Constraints as Assert,
    Doctrine\Common\Collections\ArrayCollection,
    Shopware\Components\Model\ModelEntity,
    Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="sysg_mediacapture_cache", uniqueConstraints={
 *     @ORM\UniqueConstraint(name="key_idx", columns={"cache_key"})
 * }, indexes={
 *     @ORM\Index(name="cache_tag_idx", columns={"cache_tag"}),
 *     @ORM\Index(name="accessed_at_idx", columns={"accessed_at", "created_at"})
 * })
 */
class Cache extends ModelEntity {
    /**
     * @var integer $id
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    public function getId() { return $this->id; }

    /**
     * @var string $key
     * @ORM\Column(name="cache_key", type="string", length=32, nullable=false)
     */
    private $key;

    public function getKey() { return $this->key; }
    public function setKey($key) { $this->key = $key; return $this; }

    /**
     * @var string $cacheTag
     * @ORM\Column(name="cache_tag", type="string", length=32, nullable=false)
     */
    private $cacheTag;

    public function getCacheTag() { return $this->cacheTag; }
    public function setCacheTag($cacheTag) { $this->cacheTag = $cacheTag; return $this; }

    /**
     * @var array $options
     * @ORM\Column(name="options", type="text", nullable=false)
     */
    private $options;

    public function getOptions() { return json_decode($this->options, true); }
    public function setOptions($options) { $this->options = json_encode($options); return $this; }

    /**
     * @var string $data
     * @ORM\Column(name="data", type="text", nullable=true)
     */
    private $data;

    public function getData() { return json_decode($this->data, true); }
    public function setData($data) { $this->data = json_encode($data); return $this; }

    /**
     * @var \DateTime $createdAt
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    public function getCreatedAt() { return $this->createdAt; }

    /**
     * @var \DateTime $accessedAt
     * @ORM\Column(name="accessed_at", type="datetime", nullable=false)
     */
    private $accessedAt;

    public function getAccessedAt() { return $this->accessedAt; }

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime("now");
        $this->accessedAt = new \DateTime("now");
    }

    /**
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->accessedAt = new \DateTime("now");
    }

    public function toArray()
    {
        $result = [];

        foreach(get_class_methods($this) as $methodName) {
            if (strpos($methodName, 'get') === 0) {
                $key = lcfirst(substr($methodName, 3));
                $result[$key] = $this->$methodName();
            }
        }

        return $result;
    }
}