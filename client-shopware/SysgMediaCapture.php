<?php

namespace SysgMediaCapture;

use Shopware\Components\Plugin;
use Shopware\Components\Plugin\Context\InstallContext;
use Shopware\Components\Plugin\Context\UninstallContext;

use Doctrine\ORM\Tools\SchemaTool;

class SysgMediaCapture extends Plugin
{
    private static $controller;

    public static function getController() {
        return self::$controller;
    }

    public static function getSubscribedEvents() {
        return [
            'Enlight_Bootstrap_InitResource_sysg_media_capture.api_client' => 'onInitApiClient',
            'Enlight_Bootstrap_InitResource_sysg_media_capture.cache' => 'onInitCache',
            'Enlight_Bootstrap_InitResource_sysg_media_capture.options_parser' => 'onInitOptionsParser',

            'Enlight_Controller_Action_PreDispatch => onPreDispatch',
            'Enlight_Controller_Action_PostDispatchSecure' => 'onPostDispatchSecure'
        ];
    }



    protected function getClassMetadata()
    {
        $em = $this->container->get('models');
        $classes = [
            $em->getclassMetadata(Models\Cache::class)
        ];
        return $classes;
    }

    protected function getConfig()
    {
        static $config = null;
        if ($config === null) {
            $shop = null;
            if ($this->container->has('shop')) {
                $shop = $this->container->get('shop');
            }

            if (!shop) {
                $shop = $this->container->get('models')->getRepository(\Shopware\Models\Shop\Shop::class)->getActiveDefault();
            }

            $configReader = $this->container->get('shopware.plugin.cached_config_reader');
            $config = $configReader->getByPluginName($this->getName(), $shop);
        }

        return $config;
    }

    public function install(InstallContext $context)
    {
        $em = $this->container->get('models');
        $tool = new SchemaTool($em);
        $classes = $this->getClassMetadata();
        $tool->createSchema($classes);

        parent::install($context);
    }

    public function uninstall(UninstallContext $context)
    {
        $em = $this->container->get('models');
        $tool = new SchemaTool($em);
        $classes = $this->getClassMetadata();
        $tool->dropSchema($classes);

        parent::uninstall($context);
    }



    public function onInitApiClient() {
        $config = $this->getConfig();

        $client = new Components\ApiClient(
            $this->container->get('http_client'),
            $config['key'],
            $config['secret']
        );

        return $client;
    }

    public function onInitCache() {
        $cache = new Components\Cache(
            $this->container->get('models')
        );

        return $cache;
    }

    public function onInitOptionsParser() {
        $optionsParser = new Components\OptionsParser();
        return $optionsParser;
    }




    public function onPreDispatch(\Enlight_Event_EventArgs $args)
    {
        self::$controller = $args->getSubject();
    }

    public function onPostDispatchSecure(\Enlight_Event_EventArgs $args) {
        self::$controller = $args->getSubject();

        $template = $this->container->get('Template');
        $template->addPluginsDir($this->getPath() . '/Resources/views/_private/smarty/');
    }
}
