<?php
namespace SysgMediaCapture\Components;

class OptionsParser {

    protected $_copyOptions = [
        'waitUntil',
        'delay',
        'device',
        'format',
        'landscape',
        'includeHeaderFooter',
        'emulateScreen',
        'pages',
        'margin',
        'quality',
        'area'
    ];

    protected $_copyActionParams = [
        'id',
        'class',
        'style',
        'width',
        'height',
        'alt',
        'target'
    ];


    public function __construct() {

    }

    public function getOptions($params, $content) {
        $options = [];

        if (isset($params['url'])) {
            $options['url'] = $params['url'];
        } elseif (isset($params['module']) || isset($params['controller']) || isset($params['action'])) {
            $options['url'] = Shopware()->Front()->Router()->assemble([
                'module' => isset($params['module']) ? $params['module'] : null,
                'controller' => isset($params['controller']) ? $params['controller'] : null,
                'action' =>  isset($params['action']) ? $params['action'] : null,
                'params' => isset($params['params']) ? $params['params'] : []
            ]);
        } elseif (isset($params['template'])) {
            $renderParams = isset($params['params']) ? $params['params'] : [];
            $renderParams['tpl'] = $params['template'];
            $options['url'] = Shopware()->Front()->Router()->assemble([
                'module' => 'widgets',
                'controller' => 'SysgMediaCapture',
                'action' => 'render',
                'params' => $renderParams
            ]);
        } elseif (isset($content)) {
            $options['content'] = $content;
        }

        
        foreach($this->_copyOptions as $prop) {
            if (isset($params[$prop]) && $params[$prop]) {
                $options[$prop] = $params[$prop];
            }
        }


        return $options;
    }

    public function getActionParams($params, $content) {
        $actionParams = [];

        if (isset($params['title'])) {
            $actionParams['title'] = $params['title'];
        } elseif (isset($params['url']) || isset($params['module']) || isset($params['controller']) || isset($params['action']) || isset($params['template'])) {
            if ($content) {
                $actionParams['title'] = $content;
            }
        }

        foreach($this->_copyActionParams as $prop) {
            if (isset($params[$prop]) && $params[$prop]) {
                $actionParams[$prop] = $params[$prop];
            }
        }

        return $actionParams;
    }
}