<?php
namespace SysgMediaCapture\Components;

use Shopware\Components\HttpClient\RequestException;

class ApiClient {
    const BASE_URL = 'http://icw-672-saas.makrohost-server.de';

    private $client;
    private $key;
    private $secret;
    
    private $token;


    public function __construct($client, $key, $secret)
    {
        $this->client = $client;
        $this->key = $key;
        $this->secret = $secret;
        $this->token = null;
    }

    private function login() {
        if ($this->token) {
            return;
        }

        $res = $this->client->post(self::BASE_URL . '/login', [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json'
            ], json_encode([
            'key' => $this->key,
            'secret' => $this->secret
        ]));

        $data = json_decode($res->getBody(), true);

        $this->token = $data['token'];
    }

    public function pdf($options) {
        $this->login();

        $res = $this->client->post(self::BASE_URL . '/media-capture/v1/pdf', [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'X-Access-Token' => $this->token
            ], json_encode($options));

        $data = json_decode($res->getBody(), true);

        return $data['url'];
    }

    public function image($options) {
        $this->login();

        $res = $this->client->post(self::BASE_URL . '/media-capture/v1/image', [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                'X-Access-Token' => $this->token
            ], json_encode($options));

        $data = json_decode($res->getBody(), true);

        return $data['url'];
    }

    public function exists($url) {
        try {
            $res = $this->client->head($url);
            return true;
        } catch(RequestException $e) {
            if ($e->getCode() == 404) {
                return false;
            }

            throw $e;
        }
    }
}