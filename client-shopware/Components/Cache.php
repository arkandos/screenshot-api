<?php
namespace SysgMediaCapture\Components;

use \SysgMediaCapture\Models\Cache as CacheModel;

class Cache {
    private $em;

    public function __construct($em) {
        $this->em = $em;
    }

    public function get($options, $dataOnly = true) {
        $key = $this->getKey($options);
        $entry = $this->em->getRepository(CacheModel::class)->findOneBy(['key' => $key]);

        if ($entry) {
            if ($dataOnly) {
                return $entry->getData();
            } else {
                return $entry->toArray();
            }
        } else {
            return null;
        }
    }

    public function post($options, $data) {
        $key = $this->getKey($options);

        $entry = $this->em->getRepository(CacheModel::class)->findOneBy(['key' => $key]);
        if (!$entry) {
            $entry = new CacheModel();
            $entry->setKey($key);
            $entry->setOptions($options);
            $entry->setData($data);
        }

        $entry->setCacheTag($this->getCacheTag($entry->getCacheTag()));

        $this->em->persist($entry);
        $this->em->flush();

        return $key;
    }

    public function put($options, $data) {
        $key = $this->getKey($options);

        $entry = $this->em->getRepository(CacheModel::class)->findOneBy(['key' => $key]);
        if (!$entry) {
            $entry = new CacheModel();
            $entry->setKey($key);
            $entry->setOptions($options);
        }

        $entry->setCacheTag($this->getCacheTag($entry->getCacheTag()));
        $entry->setData($data);

        $this->em->persist($entry);
        $this->em->flush();

        return $key;
    }

    public function delete($cacheTags = null)
    {
        $query = $this->em->createQueryBuilder();
        $query->delete('c')->from(\SysgMediaCapture\Models\Cache::class, 'c');

        if ($cacheTags) {
            if (!is_array($cacheTags)) {
                $cacheTags = explode(';', $cacheTags);
            }

            foreach($cacheTags as $i => $tag) {
                $query->orWhere("c.cacheTag LIKE :tag$i");
                $query->setParameter("tag$i", $tag);
            }
        }

        $query->getQuery()->execute();
    }

    private function getKey($options) {
        if (is_string($options)) {
            return $options;
        } else {
            ksort($options);
            $key = md5(json_encode($options));
            return $key;
        }
    }

    private function getCacheTag($cacheTag) {
        $cacheTag = $cacheTag ?: '';

        $context = Shopware()->Container()->get('shopware_storefront.context_service')->getShopContext();
        $action = \SysgMediaCapture\SysgMediaCapture::getController();

        if (!$action || !Shopware()->Container()->has('http_cache.cache_id_collector')) {
            return $cacheTag;
        }

        $cacheCollector = Shopware()->Container()->get('http_cache.cache_id_collector');
        $cacheIds = $cacheCollector->getCacheIdsFromController($action, $context);
        $cacheIds = Shopware()->Events()->filter(
                'Shopware_Plugins_HttpCache_GetCacheIds',
                $cacheIds,
                ['subject' => $this, 'action' => $action]
            );

        $cacheIds = array_merge($cacheIds, explode(';', $cacheTag));
        $cacheIds = array_unique(array_filter($cacheIds));

        return implode(';', $cacheIds);
    }
}
