{block name="frontend_widgets_sysg_media_capture_image"}
    {if $title}
        <img
            {if $id}id="{$id}"{/if}
            {if $class}class="{$class}"{/if}
            {if $style}style="{$style}"{/if}
            {if $width}width="{$width}"{/if}
            {if $height}height="{$height}"{/if}
            {if $alt}alt="{$alt}"{else}alt="{$title}"{/if}
            title="{$title}"           
            src="{$url}" />
    {else}
        {$url}
    {/if}
{/block}
