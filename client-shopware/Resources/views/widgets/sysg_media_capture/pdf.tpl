{block name="frontend_widgets_sysg_media_capture_pdf"}
    {if $title}
        <a
            {if $id}id="{$id}"{/if}
            {if $class}class="{$class}"{/if}
            {if $style}style="{$style}"{/if}
            {if $target}target="{$target}"{/if}
            {if $alt}alt="{$alt}"{else}alt="{$title}"{/if}
            href="{$url}">
                {$title}
        </a>
    {else}
        {$url}
    {/if}
{/block}
