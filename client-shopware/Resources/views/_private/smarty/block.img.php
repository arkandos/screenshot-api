<?php

/*
{pdf url="http://sysgrade.de/" text="Sysgrade"}
{pdf module="widget" controller="MyController" action="render" params=["id" => 5]}Link text{/pdf}
{pdf text="Link Text"}
    Some <h1>content</h1> I want to render
{/pdf}
*/

function smarty_block_img(array $params, $content, Smarty_Internal_Template $template, &$repeat)
{
    $smarty = Shopware()->Container()->get('Template');
    $parser = Shopware()->Container()->get('sysg_media_capture.options_parser');
    $cache = Shopware()->Container()->get('sysg_media_capture.cache');

    if (isset($content)) {
        $options = $parser->getOptions($params, $content);
        $actionParams = $parser->getActionParams($params, $content);

        $key = $cache->post($params);
        $actionParams['key'] = $key;


        $smarty_plugin_action = $smarty->registered_plugins[Smarty::PLUGIN_FUNCTION]['action'][0];
        if (!$smarty_plugin_action) {
            $smarty->loadPlugin('smarty_function_action');
            $smarty_plugin_action = 'smarty_function_action';
        }

        return call_user_func($smarty_plugin_action, [
            'module' => 'widgets',
            'controller' => 'SysgMediaCapture',
            'action' => 'image',
            'params' => $actionParams
        ], $template);
    }
}
