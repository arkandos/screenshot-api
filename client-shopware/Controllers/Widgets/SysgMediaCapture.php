<?php

class Shopware_Controllers_Widgets_SysgMediaCapture extends Enlight_Controller_Action {
    
    private $cache;
    private $apiClient;

    public function preDispatch() {
        $pluginPath = Shopware()->Container()->getParameter('sysg_media_capture.plugin_dir');

        $this->cache = $this->get('sysg_media_capture.cache');
        $this->apiClient = $this->get('sysg_media_capture.api_client');

        $this->get('template')->addTemplateDir($pluginPath . '/Resources/views/');
    }

    public function pdfAction() {
        $this->_generateAction('pdf');
    }

    public function imageAction() {
        $this->_generateAction('image');
    }

    private function _generateAction($type) {
        $params = $this->Request()->getParams();
        $key = $params['key'];

        $entry = $this->cache->get($key, false);

        $url = isset($entry['data']) ? $entry['data'] : null;
        if (!$url || !$this->apiClient->exists($url)) {
            $url = $this->apiClient->$type($entry['options']);
        }

        $this->cache->put($entry['options'], $url);
        
        foreach($params as $key => $val) {
            $this->View()->assign($key, $val);
        }
        $this->View()->assign('url', $url);
    }

    public function renderAction() {
        $params = $this->Request()->getParams();
        $template = $params['tpl'];

        $eventArgs = [
            'subject' => $this,
            'template' => $template,
            'params' => $params,
            'view' => $this->View()
        ];

        $this->get('events')->notify('SysgMediaCapture_RenderTemplate', $eventArgs);
        $this->get('events')->notify('SysgMediaCapture_RenderTemplate_' . $template, $eventArgs);

        foreach($params as $key => $val) {
            $this->View()->assign($key, $val);
        }
        $this->View()->assign('template', $template);
    }
}
