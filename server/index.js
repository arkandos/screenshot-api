const express = require('express')
const helmet = require('helmet')

const log = require('./logger')
const error = require('./errors')
const limiter = require('./rate-limit')
const auth = require('./auth')


const app = express()

const proxy = process.env.TRUST_PROXY || false
app.set('trust proxy', proxy == '1' ? true : proxy)


app.use(helmet())
app.use(log.morgan)
app.use(express.json())


app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
    next()
})

app.get('/', function(req, res) {
    res.json({
        message: "Hello, Sailor!",
        docs: "http://readthedocs.com/"
    })
})

app.options('/*', function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
    res.header('Access-Control-Allow-Headers', 'Content-Type,Content-Length,X-Access-Token')
    res.send(200).end()
})

app.post('/login', limiter, error.validateBody(auth.loginSchema), auth.login)
app.use('/media-capture', require('./media-capture'))
app.use('/account', require('./account'))


app.use(error.notFoundHandler)
app.use(error.errorHandler)


const port = process.env.HTTP_PORT || 8080
app.listen(port, function() {
    log.info("Listening on port", port)
})
