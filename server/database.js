const axios = require('axios')
const log = require('./logger')
const { md5 } = require('./util')

const connectionString = process.env.COUCHDB_URL || 'http://localhost:5984/'

const client = axios.create({
    baseURL: connectionString,
    timeout: 1000,
    validateStatus(status) {
        // additionally accept 304 as valid.
        return status >= 200 && status < 300 && status != 304
    }
})

const files = 'files'
const users = 'users'

async function getRevisionOrNull(db, docId) {
    /**
     * This version is not supported by pouchdb-server
     */

    // const url = `/${encodeURIComponent(db)}/${encodeURIComponent(docId)}`
    // try {
    //     const res = await client.head(url)
    //     return JSON.parse(res.headers.ETag)
    // } catch(err) {
    //     if (err == 404) return null
    //     else throw err
    // }

    const url = `/${encodeURIComponent(db)}/_all_docs`
    const res = await client.get(url, {
        params: { key: JSON.stringify(docId) }
    })

    if (res.data.rows.length == 1) {
        return res.data.rows[0].value.rev
    } else {
        return null
    }
}

async function getDocumentOrNull(db, docId) {
    const url = `/${encodeURIComponent(db)}/${encodeURIComponent(docId)}`

    try {
        const res = await client.get(url)
        return res.data
    } catch(err) {
        if (err.status == 404) return null
        else throw err
    }
}

async function getAttachmentOrNull(db, docId, attname) {
    const url = `/${encodeURIComponent(db)}/${encodeURIComponent(docId)}/${encodeURIComponent(attname)}`
    try {
        const res = await client.get(url, {
            responseType: 'stream'
        })
        return res.data
    } catch(err) {
        if (err.status == 404) {
            return null
        } else {
            throw err
        }
    }
}

async function getView(db, ddoc, view, params) {
    const url = `/${encodeURIComponent(db)}/_design/${encodeURIComponent(ddoc)}/_view/${encodeURIComponent(view)}`
    const res = await client.get(url, { params })
    return res.data
}

async function insertDocument(db, data) {
    if (data._id) {
        const url = `/${encodeURIComponent(db)}/${encodeURIComponent(data._id)}`
        while(true) {
            let rev = await getRevisionOrNull(db, data._id)

            try {
                const res = await client.put(url, data, {
                    params: { rev }
                })

                if (res.data.ok) {
                    data._id = res.data.id
                    data._rev = res.data.rev
                    break
                }
            } catch(err) {
                if (err.status == 409) continue
                else throw err
            }
        }
    } else {
        const url = `/${encodeURIComponent(db)}`
        const res = await client.post(url, data)
        if (!res.data.ok) {
            throw new Error("Could not insert Document!")
        }

        data._id = res.data.id
        data._rev = res.data.rev
    }

    return data
}

async function updateDocument(db, docId, updateFn) {
    const url = `/${encodeURIComponent(db)}/${docId}`
    while(true) {
        const existing = await client.get(url)
        const updated = updateFn(existing.data)
        try {
            const result = await client.put(url, updated, {
                params: {
                    rev: existing.data._rev
                }
            })
            if (result.data.ok) {
                updated._id = result.data.id
                updated._rev = result.data.rev
                return updated
            }
        } catch(err) {
            if (err.status == 409) continue
            else throw err
        }
    }
}

async function insertAttachment(db, docId, attname, mime, data) {
    const url = `/${db}/${docId}/${attname}`
    while(true) {
        let rev = await getRevisionOrNull(db, docId)
        if (!rev) {
            throw new Error("Document " + docId + " not found!")
        }

        try {
            const res = await client.put(url, data, {
                params: { rev },
                headers: {
                    'Content-Type': mime
                }
            })
            if (res.data.ok) {
                return res.data.rev
            }
        } catch(err) {
            if (err.status == 409) continue
            else throw err
        }
    }
}

async function getAccessKeyOrNull(key) {
    const opts = { key, limit: 2 }
    const body = await getView(users, 'users', 'access', { key, limit: 2 })
    if (body && body.rows.length === 1) {
        return body.rows[0].value
    }
    return null
}

async function getUserByKeyOrNull(key) {
    const body = await getView(users, 'users', 'access', { key, include_docs: true, limit: 2 })
    if (body && body.rows.length === 1) {
        return body.rows[0].doc
    }
    return null
}

function getFileOrNull(id) {
    return getDocumentOrNull(files, 'file-' + id)
}

async function getFileAndStreamOrNull(id) {
    const doc = await getFileOrNull(id)
    if (doc) {
        doc.stream = await getAttachmentOrNull(files, doc._id, id)
    }
    return doc
}

async function insertFile(uid, options, data, mime) {
    const fid = md5(options)
    const id = `${uid}${fid}`

    const doc = {
        ...options,
        _id: 'file-' + id,
        id,
        uid,
        fid,
        mime
    }

    const newFile = await insertDocument(files, doc)
    log.debug("Added file", newFile)

    await insertAttachment(files, newFile._id, id, mime, data)

    return id
}

async function insertUser(userDoc) {
    const result = await insertDocument(users, userDoc)
    return result._id
}


module.exports = {
    getAccessKeyOrNull,
    getUserByKeyOrNull,
    insertFile,
    insertUser,
    getFileAndStreamOrNull
}
