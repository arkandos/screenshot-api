const path = require('path')
const winston = require('winston')
const morgan = require('morgan')

winston.emitErrs = true

const logger = new winston.Logger({
    transports: [
        new winston.transports.File({
            level: 'info',
            filename: path.join(__dirname, 'log/info.log'),
            handleExceptions: true,
            json: true,
            maxSize: 5242880, // 5MB
            maxFiles: 5,
            colorize: false
        }),
        new winston.transports.Console({
            level: 'debug',
            handleExceptions: true,
            json: false,
            colorize: true,
            timestamp: true
        })
    ],
    exitOnError: false
})

const stream = {
    write(message) {
        logger.info(message.trim())
    }
}

function exception(msg, ex) {
    logger.error(msg, winston.exception.getAllInfo(ex))
}

module.exports = logger
module.exports.exception = exception
module.exports.stream = stream
module.exports.morgan = morgan('short', { stream })
