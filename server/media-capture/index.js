const express = require('express')

const auth = require('../auth')
const error = require('../errors')
const limiter = require('../rate-limit')

const devices = require('./devices')
const { image, imageSchema } = require('./image')
const { pdf, pdfSchema } = require('./pdf')

const api = express.Router()

api.get('/v1/devices', auth.requireRole('media-capture'), devices.devices)
api.get('/v1/device/:name', auth.requireRole('media-capture'), devices.device)

api.post('/v1/image', auth.requireRole('media-capture'), limiter, error.validateBody(imageSchema), image)
api.post('/v1/pdf', auth.requireRole('media-capture'), limiter, error.validateBody(pdfSchema), pdf)


api.get('/v1/get/:token', auth.requireToken(token => Boolean(token.key)), require('./get'))


module.exports = api
