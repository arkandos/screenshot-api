const error = require('../errors')
const log = require('../logger')
const { isObject, isString, merge, wrapAsync } = require('../util')
const { loadPage, loadPageSchema } = require('./generate')


const imageSchema = merge(loadPageSchema, {
    properties: {
        format: { enum: ['jpeg', 'png'], default: 'jpeg' },
        quality: { type: 'integer', exclusiveMinimum: 0, maximum: 100, default: 70 },
        area: { oneOf: [
            { type: 'string' },
            {
                type: 'object',
                properties: {
                    x: { type: 'number' },
                    y: { type: 'number' },
                    width: { type: 'number' },
                    height: { type: 'number' }
                },
                required: ['x', 'y', 'width', 'height'],
                additionalProperties: false
            }
        ] }
    },
    required: [ 'format' ]
})

const image = wrapAsync(async function _image(req, res) {
    const loadOptions = {
        uid: req.user.uid,
        extension: req.body.format === 'jpeg' ? '.jpg' : '.png',
        mime: req.body.format === 'jpeg' ? 'image/jpeg' : 'image/png'
    }

    const relativeUrl = await loadPage(loadOptions, req.body, async function _renderImage(page) {
        const screenshotOptions = {}
        screenshotOptions.type = req.body.format
        if (screenshotOptions.type === 'jpeg') {
            screenshotOptions.quality = req.body.quality
        }
        if (isObject(req.body.area)) {
            screenshotOptions.clip = req.body.area
        } else if (!isString(req.body.area)) {
            screenshotOptions.fullPage = true
        }

        log.debug("image", req.body.area || "page", screenshotOptions)

        const element = await (isString(req.body.area) ? page.$(req.body.area) : Promise.resolve(page))
        if (!element) {
           error.selectorNotFound({ selector: req.body.area })
        }

        return await element.screenshot(screenshotOptions)
    });

    const url = 'http://' + req.headers.host + req.baseUrl + relativeUrl
    res.json({ url })
})

module.exports = {
    imageSchema,
    image
}
