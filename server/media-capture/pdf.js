const log = require('../logger')
const { isObject, isString, merge, wrapAsync } = require('../util')
const { loadPage, loadPageSchema } = require('./generate')

const unitSchema = { oneOf: [
    { type: 'number', minimum: 0 },
    { type: 'string', pattern: '^[0-9]+(\.[0-9]+)?(px|in|cm|mm)$' }]
}

const pdfSchema = merge(loadPageSchema, {
    properties: {
        format: {
            oneOf: [
                {
                    type: 'string',
                    enum: [
                        'Letter', 'Legal', 'Tabloid', 'Ledger', 'A0', 'A1', 'A2', 'A3', 'A4', 'A5', 'A6'
                    ],
                },
                {
                    type: 'object',
                    properties: {
                        width: unitSchema,
                        height: unitSchema
                    },
                    required: [ 'width', 'height' ],
                    additionalProperties: false
                }
            ],
            default: 'A4'
        },
        landscape: { type: 'boolean', default: false },
        includeHeaderFooter: { type: 'boolean', default: false },
        emulateScreen: { type: 'boolean', default: false },
        pages: {
            type: 'array',
            minItems: 1,
            items: { type: 'integer', minimum: 0 }
        },
        margin: {
            type: 'object',
            properties: {
                left: unitSchema,
                right: unitSchema,
                bottom: unitSchema,
                top: unitSchema
            },
            additionalProperties: false
        }
    },
    require: [ 'format' ]
})

const pdf = wrapAsync(async function _pdf(req, res) {
    const loadOptions = {
        uid: req.user.uid,
        extension: '.pdf',
        mime: 'application/pdf'
    }

    const relativeUrl = await loadPage(loadOptions, req.body, async function _renderPdf(page) {
        const pdfOptions = {}
        pdfOptions.landscape = req.body.landscape
        pdfOptions.displayHeaderFooter = req.body.includeHeaderFooter
        pdfOptions.margin = req.body.margin
        if (req.body.pages) {
            pdfOptions.pageRanges = req.body.pages.join()
        }
        if (isString(req.body.format)) {
            pdfOptions.format = req.body.format
        } else if (isObject(req.body.format)) {
            pdfOptions.width = '' + req.body.format.width
            pdfOptions.height = '' + req.body.format.height
        }

        log.debug("pdf", pdfOptions)

        if (req.body.emulateScreen) {
            await page.emulateMedia('screen')
        }
        return await page.pdf(pdfOptions)
    })

    const url = 'http://' + req.headers.host + req.baseUrl + relativeUrl
    res.json({ url })
})

module.exports = {
    pdfSchema,
    pdf
}
