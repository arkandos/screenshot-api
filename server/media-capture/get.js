const log = require('../logger')
const error = require('../errors')
const { getFileAndStreamOrNull } = require('../database')
const { wrapAsync } = require('../util')

const retrieve = wrapAsync(async function _retrieve(req, res) {
    const key = req.user.key
    const file = await getFileAndStreamOrNull(key)
    if (!file) {
        error.notFound()
    }

    res.set('Content-Type', file.mime)
    file.stream.pipe(res)
})

module.exports = retrieve;
