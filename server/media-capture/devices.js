const pool = require('../browser-pool')
const error = require('../errors')

function devices(req, res) {
    res.json({
        data: pool.deviceNames,
        total: pool.deviceNames.length
    })
}

function device(req, res) {
    const deviceName = req.params.name

    if (!pool.devices.hasOwnProperty(deviceName)) {
        errors.notFound({ device: deviceName })
    }

    res.json(pool.devices[deviceName])
}

module.exports = {
    device,
    devices
}
