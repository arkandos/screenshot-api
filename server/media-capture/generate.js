const { URL } = require('url')
const jwt = require('jsonwebtoken')

const error = require('../errors')
const pool = require('../browser-pool')
const { insertFile } = require('../database')
const { delay, md5 } = require('../util')

const loadWaitUntilOptions = ['load', 'domcontentloaded', 'networkidle0', 'networkidle2']
const navigationTimeout = (1*process.env.BROWSER_NAVIGATION_TIMEOUT) || 15000
const waitTimeout = (1*process.env.BROWSER_WAIT_TIMEOUT) || 5000

const timeoutSentinel = {}

const loadPageSchema = {
    type: 'object',
    properties: {
        auth: {
            type: 'object',
            properties: {
                username: { type: 'string' },
                password: { type: 'string' }
            },
            required: [ 'username', 'password' ],
            additionalProperties: false
        },
        url: { type: 'string', format: 'uri', pattern: '^https?:\/\/' },
        content: { type: 'string' },
        waitUntil: { type: 'string', default: 'load' },
        delay: { type: 'integer', minimumExclusive: 0, maximum: waitTimeout },
        device: { enum: pool.deviceNames, default: 'Full HD' }
    },
    dependencies: { auth: ['url'] },
    allOf: [{
        oneOf: [
            { required: ['url'] },
            { required: ['content'] }
        ],
    }]
}


/**
 * @description Loads a page and calls renderFn to render it
 * @returns A url to the rendered buffer resource.
 * @param {*} loadOptions Internal options passed to this function
 *   - uid: User ID
 *   - extension: Extension to use for the generated file.
 *   - mime: Mime type of the result of the render function
 * @param {*} userOptions User-defined options (see schema)
 * @param {*} renderFn Callback used to render the thing into a Buffer.
 *   - page: puppeteer.Page Object.
 *   - userOptions: pass-through
 */
async function loadPage(loadOptions, userOptions, renderFn) {
    const data = await pool.use(loadOptions.uid, async function _usePool(page) {
        await page.emulate(pool.devices[userOptions.device])
        await page.authenticate(userOptions.auth)

        const loadOptions = {
            timeout: navigationTimeout,
            waitUntil: loadWaitUntilOptions.indexOf(userOptions.waitUntil) >= 0 ? userOptions.waitUntil : 'load'
        }

        let response = null
        try {
            if (userOptions.url) {
                response = await page.goto(userOptions.url, loadOptions)
            } else {
                response = await Promise.race([
                    page.setContent(userOptions.content, loadOptions),
                    delay(loadOptions.timeout, timeoutSentinel)
                ])
            }
        } catch(err) {
            if (err.message === 'net::ERR_INSECURE_RESPONSE') {
                error.sslError({ url: userOptions.url })
            } else {
                error.timeoutExceeded({ url: userOptions.url, timeout: loadOptions.timeout, originalMessage: err.message })
            }
        }
        if (response === timeoutSentinel) {
            error.timeoutExceeded({ content: userOptions.content, timeout: loadOptions.timeout })
        }
        if (response && !response.ok) {
            error.badRequest({ url: response.url, status: response.status })
        }

        if (loadWaitUntilOptions.indexOf(userOptions.waitUntil) < 0) {
            try {
                await page.waitForSelector(userOptions.waitUntil, { timeout: waitTimeout })
            } catch(err) {
                error.selectorNotFound({ selector: userOptions.waitUntil })
            }
        }

        await delay(Math.max(userOptions.delay, waitTimeout))

        return await renderFn(page, userOptions)
    })

    if (!data || !Buffer.isBuffer(data)) {
        error.renderError()
    }

    const key = await insertFile(loadOptions.uid, userOptions, data, loadOptions.mime)
    const signedKey = jwt.sign({ key }, process.env.SECRET, { noTimestamp: true })

    return '/v1/get/' + signedKey + '?' + loadOptions.extension
}

module.exports = {
    loadPageSchema,
    loadPage
}
