const Ajv = require('ajv')

const log = require('./logger')
const { getClassName, lcfirst, merge } = require('./util')

class ResponseError extends Error {
    constructor(code, message, status, response) {
        super(message)
        this.code = code
        this.status = status
        this.response = response
    }
}

function errorHandler(err, req, res, next) {
    let response = { message: err.message, error: lcfirst(getClassName(err)) }
    let status = 500

    if (err instanceof ResponseError) {
        response = Object.assign(response, err.response, { error: err.code })
        status = err.status || 400
    } else if (err instanceof SyntaxError) {
        response.error = 'syntaxError'
        status = 400
    }

    if (err instanceof Error) {
        log.exception(`${req.method} ${req.path} failed:`, err)
    } else {
        log.error(`${req.method} ${req.path} failed:`, err)
    }

    res.status(status).json(response)
}

function notFoundHandler(req, res, next) {
    notFound({ path: req.path })
}



const ajv = new Ajv({ useDefaults: true, allErrors: true })

function validateBody(schema) {
    schema = merge({
        additionalProperties: false
    }, schema)

    const validate = ajv.compile(schema)

    return function(req, res, next) {
        if (!validate(req.body)) {
            const errorMessages = validate.errors.map(err => ({
                code: err.keyword,
                path: err.dataPath,
                message: err.message
            }))
            validationFailed({ errors: errorMessages })
        }
        next()
    }
}


function _raise(code, message, status, responseDefaults) {
    return function (response) {
        response = Object.assign({}, responseDefaults, response);
        throw new ResponseError(code, message, status, response);
    }
}

const noAuth = _raise('noAuth', "Not authenticated", 401)
const notFound = _raise('notFound', "Not found.", 404)
const rateLimit = _raise('rateLimit', "Too many requests.", 429)
const validationFailed = _raise('validationFailed', "Validation Failed.", 400)
const badRequest = _raise('badRequest', "Bad request", 422)
const renderError = _raise('renderError', "Could not render page.", 422)
const selectorNotFound = _raise('selectorNotFound', "Selector not found in page.", 422)
const timeoutExceeded = _raise('timeout', "Could not load page in time.", 422)
const sslError = _raise('insecure', "Certificate of requested page is not valid.", 422)


module.exports = {
    ResponseError,
    errorHandler,
    notFoundHandler,
    validateBody,
    noAuth,
    notFound,
    rateLimit,
    validationFailed,
    badRequest,
    renderError,
    selectorNotFound,
    timeoutExceeded,
    sslError,
}
