const path = require('path')
const puppeteer = require('puppeteer')
const devices = require('puppeteer/DeviceDescriptors')
const AsyncCache = require('async-cache')

const error = require('./errors')
const log = require('./logger')
const { delay, promisifyObject } = require('./util')


const cache = promisifyObject(new AsyncCache({
    max: parseInt(process.env.BROWSER_CACHE_SIZE) || 10,
    maxAge: parseInt(process.env.BROWSER_CACHE_AGE) || (24 * 60 * 60),

    async load(key, cb) {
        log.info("Launching new browser instance for", key)
        try {
            const browser = await puppeteer.launch({
                // we are already inside a sandbox.
                args: ['--no-sandbox'],
                userDataDir: path.join(path.dirname(require.main.filename), 'user-profiles', key)
            })
            cb(null, browser)
        } catch(err) {
            cb(err)
        }
    },

    async dispose(key, browser) {
        try {
            log.info("Closing browser for", key)
            await browser.close()
        } catch(err) {
            log.error("Error closing browser:", err.message)
        }
    }
}))


const maxUseTime = (1*process.env.BROWSER_TIMEOUT) || (30*1000)
const timeoutSentinel = {};

async function use(key, callback) {
    const browser = await cache.get(key)
    const page = await browser.newPage()

    let result = null
    try {
        result = await Promise.race([
            callback(page, browser),
            delay(maxUseTime, timeoutSentinel)
        ]);

        if (result === timeoutSentinel) {
            error.timeoutExceeded()
        }

        await page.close()
    } catch(err) {
        if (!(err instanceof error.ResponseError) || err.code === 'timeout') {
            log.error("Error using browser, closing:", err.message)
            cache.del(key)
        }
        throw e;
    }

    return result
}

devices['Full HD'] = {
    name: 'Full HD',
    userAgent: "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3236.0 Safari/537.36",
    viewport: {
        width: 1920,
        height: 1080,
        deviceScaleFactor: 1,
        isMobile: false,
        hasTouch: false,
        isLandscape: true
    }
};
devices['HD Ready'] = {
    name: 'HD Ready',
    userAgent: "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3236.0 Safari/537.36",
    viewport: {
        width: 1280,
        height: 720,
        deviceScaleFactor: 1,
        isMobile: false,
        hasTouch: false,
        isLandscape: true
    }
};

function close() {
    cache.reset()
}

const deviceNames = Object.keys(devices).filter(key => key !== '0' && !(1 * key))

module.exports = {
    use,
    close,
    devices,
    deviceNames
}
