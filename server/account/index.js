const express = require('express')

const auth = require('../auth')
const error = require('../errors')

const account = require('./account')
const apiKey = require('./api-key')

const api = express.Router()

api.use(auth.requireRole('account'))

api.get('/v1/me', account.getUser)
api.put('/v1/me', account.putUser)

api.get('/v1/api-keys', apiKey.getApiKeys)
api.post('/v1/api-key', error.validateBody(apiKey.createApiKeySchema), apiKey.createApiKey)
api.delete('/v1/api-key/:key([-A-Za-z0-9+/=]+)', apiKey.deleteApiKey)

module.exports = api
