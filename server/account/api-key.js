const bcrypt = require('bcryptjs')

const error = require('../errors')
const log = require('../logger')
const { getUserByKeyOrNull, insertUser } = require('../database')
const { isString, omit, randomString, wrapAsync } = require('../util')

const getApiKeys = wrapAsync(async function _getApiKeys(req, res) {
    const userDoc = await getUserByKeyOrNull(req.user.sub)
    if (!userDoc) {
        error.noAuth()
    }

    const apiKeys = userDoc.access.map(token => omit(token, ['secret']))

    res.json({ data: apiKeys, total: apiKeys.length })
})

// TODO: Make items enum dynammic.
const createApiKeySchema = {
    properties: {
        name: { type: 'string' },
        note: { type: 'string' },
        roles: {
            type: 'array',
            minItems: 1,
            uniqueItems: true,
            items: { enum: [ 'media-capture' ] }
        }
    },
    required: ['roles']
}

const createApiKey = wrapAsync(async function _create(req, res) {
    const userDoc = await getUserByKeyOrNull(req.user.sub)
    if (!userDoc) {
        error.noAuth()
    }

    // user has to have this role to create api-tokens for it
    for(const role of req.body.roles) {
        if (!userDoc.roles.includes(role)) {
            error.noAuth()
        }
    }

    const sub = randomString(64)
    const secret = randomString(64)
    const password = bcrypt.hashSync(secret, bcrypt.genSaltSync(10))

    userDoc.access.push({
        ... req.body,
        sub,
        secret: password
    })

    await insertUser(userDoc)

    res.json({
        ... req.body,
        sub,
        secret
    })
})

const deleteApiKey = wrapAsync (async function _delete(req, res) {
    const userDoc = await getUserByKeyOrNull(req.user.sub)
    if (!userDoc) {
        error.noAuth()
    }

    const key = req.params.key
    if (!isString(key)) {
        error.validationFailed({ errors: [{code: 'required', path: '', message: "should have required property 'key'"}]})
    }

    log.debug("Removing Api-Key", key)

    for(let i = 0; i < userDoc.access.length; ++i) {
        if (userDoc.access[i].sub === key) {
            userDoc.access.splice(i, 1)
            await insertUser(userDoc)
            return res.sendStatus(200)
        }
    }

    error.notFound({ key })
})

module.exports = {
    getApiKeys,
    createApiKeySchema,
    createApiKey,
    deleteApiKey
}
