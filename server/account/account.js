const error = require('../errors')
const log = require('../logger')
const { getUserByKeyOrNull, insertUser } = require('../database')
const { merge, omit, wrapAsync } = require('../util')

function omitUserProps(user) {
    return omit(user, ['_id', '_rev', 'uid', 'access', 'secret'])
}

const getUser = wrapAsync(async function _getUser(req, res) {
    const userDoc = await getUserByKeyOrNull(req.user.sub)
    if (!userDoc) {
        error.noAuth()
    }

    res.json(omitUserProps(userDoc))
})

// TODO: Schema validation.

const putUser = wrapAsync(async function _putUser(req, res) {
    const userDoc = await getUserByKeyOrNull(req.user.sub)
    if (!userDoc) {
        error.noAuth()
    }

    userDoc = merge(userDoc, omitUserProps(req.body))

    log.info("Update User", req.user.sub, userDoc)

    await insertUser(userDoc)

    res.json(omitUserProps(userDoc))
})

module.exports = {
    getUser,
    putUser
}
