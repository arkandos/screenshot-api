const axios = require('axios')
const bcrypt = require('bcryptjs');
const { md5 } = require('../util');

const client = axios.create({
    baseURL: 'http://localhost:5984/',
    timeout: 1000
})



const name = process.argv[2];
const email = process.argv[3];
const password = process.argv[4];

const uid = md5(email)
const userDoc = {
    _id: 'user-' + uid,
    email,
    name,
    uid: uid,

    roles: ['account', 'media-capture'],

    access: [
        {
            sub: email,
            secret: bcrypt.hashSync(password, bcrypt.genSaltSync(10)),
            roles: ['account']
        }
    ]
};

insertDocument('users', userDoc).then(console.log).catch(console.error)



async function insertDocument(db, data) {
    if (data._id) {
        const url = `/${encodeURIComponent(db)}/${encodeURIComponent(data._id)}`
        while (true) {
            let rev = await getRevisionOrNull(db, data._id)

            try {
                const res = await client.put(url, data, {
                    params: { rev }
                })

                if (res.data.ok) {
                    data._id = res.data.id
                    data._rev = res.data.rev
                    break
                }
            } catch (err) {
                if (err.status == 409) continue
                else throw err
            }
        }
    } else {
        const url = `/${encodeURIComponent(db)}`
        const res = await client.post(url, data)
        if (!res.data.ok) {
            throw new Error("Could not insert Document!")
        }

        data._id = res.data.id
        data._rev = res.data.rev
    }

    return data
}

async function getRevisionOrNull(db, docId) {
    /**
     * This version is not supported by pouchdb-server
     */

    // const url = `/${encodeURIComponent(db)}/${encodeURIComponent(docId)}`
    // try {
    //     const res = await client.head(url)
    //     return JSON.parse(res.headers.ETag)
    // } catch(err) {
    //     if (err == 404) return null
    //     else throw err
    // }

    const url = `/${encodeURIComponent(db)}/_all_docs`
    const res = await client.get(url, {
        params: { key: JSON.stringify(docId) }
    })

    if (res.data.rows.length == 1) {
        return res.data.rows[0].value.rev
    } else {
        return null
    }
}
