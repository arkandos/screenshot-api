const merge = require('deepmerge');

const db = require('couchdb-promises')({
    baseUrl: process.env.COUCHDB_URL || 'http://localhost:5984/'
})

async function getOrNull(promise) {
    try {
        const result = await promise
        if (result.status == 200) {
            return result.data
        } else {
            return null
        }
    } catch(err) {
        if (err.status == 404) {
            return null
        } else {
            throw err
        }
    }
}

async function createDbIfNotExists (dbname) {
    try {
        await db.createDatabsae(dbname)
        console.log("Database created:", dbname)
    } catch(err) {
        console.log("Database", dbname, "not created:", err.message)
    }
}

async function createViewIfNotExists(dbname, designDocName, viewName, map, reduce) {
    const designDoc = await getOrNull(db.getDesignDocument(dbname, designDocName))

    if (!designDoc || !designDoc.views.hasOwnProperty(viewName)) {
        console.log(`Creating view ${viewName} in design document ${designDocName}`)
        await db.createDesignDocument(dbname, merge(designDoc || {}, {
            language: 'javascript',
            views: {
                [viewName]: {
                    map: map ? map.toString() : undefined,
                    reduce: reduce ? reduce.toString() : undefined
                }
            }
        }), designDocName)
    }
}

async function upsertView(dbname, designDocName, viewName, map, reduce) {
    const designDoc = await getOrNull(db.getDesignDocument(dbname, designDocName))

    if (designDoc && designDoc.views.hasOwnProperty(viewName)) {
        console.log(`Updating view ${viewName} in design document ${designDocName}`)
    } else {
        console.log(`Creating view ${viewName} in design document ${designDocName}`);
    }

    await db.createDesignDocument(dbname, merge(designDoc || {}, {
        language: 'javascript',
        views: {
            [viewName]: {
                map: map ? map.toString() : undefined,
                reduce: reduce ? reduce.toString() : undefined
            }
        }
    }), designDocName)
}


async function main() {
    await createDbIfNotExists('users');
    await upsertView('users', 'users', 'access', function(doc) {
        if (doc.access) {
            for (var i = 0; i < doc.access.length; ++i) {
                var access = doc.access[i];
                access.uid = doc.uid;
                emit(access.sub, access);
            }
        }
    });

    await createDbIfNotExists('files');
    // await createDbIfNotExists(couch, 'requests');

    // await createViewIfNotExists(couch.use('requests'), 'cost', 'cost', function(doc) {
    //     emit([doc.uid, doc.timestamp], doc.cost);
    // }, '_sum');
}

main().catch(console.error);