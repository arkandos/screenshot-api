const ExpressBrute = require('express-brute')
const error = require('./errors')
const log = require('./logger')

// rate limiting - @TODO: Need to be saucier here.

// const requests = require('nano')('http://localhost:5984/requests/');

// const myLimiter = function(req, res, next) {
//     const user = req.user.uid;
//     const startTime = Date.now() - 15*60*1000;
//     const params = {
//         startkey: [req.user.uid, startTime],
//         endkey: [req.user.uid, {}],
//         group_level: 1,
//         reduce: true
//     };
//     requests.view('cost', 'cost', params, function(err, data) {

//     });
// }


const requestsPerBucket = process.env.RATE_LIMIT_REQUESTS || 300
const bucketLength = process.env.RATE_LIMIT_TIME || (15 * 60) // s

const limiter = new ExpressBrute(new ExpressBrute.MemoryStore(), {
    freeRetries: 1*requestsPerBucket,
    lifetime: 1*bucketLength,
    minWait: (bucketLength + 1) * 1000,
    maxWait: (bucketLength + 1) * 1000,
    attachResetToRequest: false,
    refreshTimeoutOnRequest: false,
    failCallback(req, res, next, nextValidRequestDate) {
        error.rateLimit({ nextValidRequestDate })
    }
})

const middleware = limiter.getMiddleware({
    key(req, res, next) {
        if (req.user && req.user.uid) {
            next(req.user.uid);
        } else {
            next();
        }
    }
})

module.exports = middleware
