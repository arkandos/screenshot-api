const crypto = require('crypto')
const merge = require('deepmerge')
const utils = require('util')

function delay(ms, value) {
    return new Promise(function(resolve, reject) {
        setTimeout(function() {
            resolve(value)
        }, ms)
    })
}

function getClassName(obj) {
    return Object.prototype.toString.call(obj).match(/^\[object\s(.*)\]$/)[1]
}

function groupBy(arr, iteratee) {
    const result = {}
    for(const elem of arr) {
        const key = iteratee ? iteratee(elem) : elem
        if (!(key in result)) {
            result[key] = [elem];
        } else {
            result[key].push(elem)
        }
    }
    return result
}

function isFunction(obj) {
    return typeof obj === 'function'
}

function isObject(obj) {
    return Object.prototype.toString.call(obj) === "[object Object]"
}

function isString(obj) {
    return (typeof obj === 'string')
}

function lcfirst(str) {
    return str.charAt(0).toLowerCase() + str.substr(1)
}

function md5(data) {
    if (!Buffer.isBuffer(data) && isObject(data)) {
        data = JSON.stringify(data)
    }
    return crypto.createHash('md5').update(data).digest('hex')
}

function omit(obj, props) {
    const result = {}
    props = groupBy(props)

    for (let key in obj) {
        if (!(key in props)) {
            result[key] = obj[key]
        }
    }
    return result
}

function pick(obj, props) {
    const result = {}
    props = groupBy(props)

    for(let key in obj) {
        if (key in props) {
            result[key] = obj[key]
        }
    }
    return result
}

function randomString(length) {
    const buf = crypto.randomBytes(Math.ceil(length / 4) * 3)
    return buf.toString('base64').substr(0, length)
}

const promisify = utils.promisify

function promisifyObject(obj, options) {
    options = options || {}

    const result = Object.create(obj)

    for(const key in obj) { // no hasOwnProperty guard here.
        const val = obj[key]

        if (isFunction(val) &&
            (!options.include || options.include.includes(key)) &&
            (!options.exclude || !options.exclude.includes(key)))
        {
            result[key] = function() {
                const self = this
                const args = Array.from(arguments)

                return new Promise(function(resolve, reject) {
                    args.push(function (err, result) {
                        if (err !== null) reject(err)
                        else resolve(result)
                    })

                    val.apply(self, args)
                })
            }
        }
    }

    return result
}

function wrapAsync(fn) {
    return function(req, res, next) {
        fn(req, res, next).catch(next)
    }
}

module.exports = {
    delay,
    getClassName,
    groupBy,
    isObject,
    isString,
    lcfirst,
    merge,
    md5,
    omit,
    pick,
    promisify,
    promisifyObject,
    randomString,
    wrapAsync
}
