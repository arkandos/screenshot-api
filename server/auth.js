const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const log = require('./logger')
const error = require('./errors')

const { getAccessKeyOrNull } = require('./database')
const { md5, pick, wrapAsync } = require('./util')

const salt = process.env.SALT || 'secret'

/**
 * Middleware function that requires routes to have a valid token passing the optional validation function.
 * @param (* -> bool) validate
 */
function requireToken(validate) {
    return function(req, res, next) {
        const token = req.headers['x-access-token'] || req.params.token || req.query.token
        log.debug("X-Access-Token: ", token)

        if (!token)  error.noAuth()

        jwt.verify(token, process.env.SECRET, function(err, decoded) {
            if (err) {
                log.error("Authentication Error:", err.message)
                error.noAuth()
            }

            if (validate && !validate(decoded))  error.noAuth()

            log.debug("Verified Key:", decoded)

            req.user = decoded
            next()
        })
    }
}

/**
 * Middleware function validating a user token.
 */
const requireLogin       =  requireToken(token => Boolean(token.uid))
function requireRole(role) {
    return requireToken(function(token) {
        return token.uid && token.roles && token.roles.includes(role)
    })
}

const loginSchema = {
    type: 'object',
    properties: {
        key: { type: 'string' },
        secret: { type: 'string' }
    },
    required: [ 'key', 'secret' ]
}

/**
 * Login route generating a user token.
 */
const login = wrapAsync(async function _login(req, res) {
    const access = await getAccessKeyOrNull(req.body.key)

    if (!access || access.roles.length === 0) {
        error.noAuth({ reason: "not found"})
    }
    if (!bcrypt.compareSync(req.body.secret, access.secret)) {
        error.noAuth({ reason: "password invalid" })
    }

    const payload = pick(access, ['sub', 'uid', 'roles'])

    /*
     * Tokens are valid until the end of the month,
     * because we bill our users monthly.
     */

    const nextMonth = new Date()
    nextMonth.setMonth(nextMonth.getMonth() + 1, 0)
    nextMonth.setHours(23, 59, 59, 999)

    const signOptions = {
        expiresIn: parseInt((nextMonth.getTime() - Date.now()) / 1000)
    }

    const token = jwt.sign(payload, process.env.SECRET, signOptions)
    res.json({
        token,
        roles: access.roles
    })
})

module.exports = {
    requireToken,
    requireLogin,
    requireRole,
    loginSchema,
    login
}
